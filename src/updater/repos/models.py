from django.conf import settings
from django.db import models
import os, re, time
import subprocess
import uuid

def _repo_meta_data_location_default():
    return "%s" % (
        os.path.join(settings.SITE_ROOT, '../', str(uuid.uuid4()))
        )

# Create your models here.
class Repo(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    file_system_location = models.CharField(max_length=255, unique=True)
    repo_meta_data_location = models.CharField(max_length=255, default=_repo_meta_data_location_default)
    last_modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s ( %s )"%(self.name, self.file_system_location)

class RepoView(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    main_repo = models.ForeignKey('Repo')
    last_modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s"%(self.name)

    def get_file_list(self):
        try:
            files = [ f for f in os.listdir("%s/repodata"%
                (self.get_repo_meta_dir())) ]
        except OSError:
            files = ['Error - meta data directory missing',
            '%s'%(self.get_repo_meta_dir()),
            ]

        if len(files) == 0:
            files = ['WARNING: NO REPO DATA EXIST!!!!!!!!']

        return files

    def get_repo_meta_dir(self):
        """
        This is the directory the will hold the "/repodata" location,
        not the actual repodata path.Firewall001
        """
        return "%s/%s"%(
            self.main_repo.repo_meta_data_location,
            self._nameToDir()
            )

    def _nameToDir(self):
        return re.sub(r'\s','',self.name.lower())

    def save(self, *args, **kwargs):
        #Here we need to update the createview, we do this every time
        #we save regardless of change.
        #Why? Because it current offers a method to force a new set
        #of repo files if RPM's have changed.

        repo_meta_data = self.get_repo_meta_dir()

        if os.path.exists(repo_meta_data):
            backup_dir = "%s.%s"%(
                repo_meta_data,
                int(time.time())
                )
            os.rename(repo_meta_data, backup_dir)

        os.makedirs(repo_meta_data)

        cmd = []
        cmd.append("%s"%(settings.CREATE_REPO))
        cmd.append("-o")
        cmd.append("%s"%(repo_meta_data))
        cmd.append("%s"%(self.main_repo.file_system_location))

        print "Running: %s"%(cmd)
        return_code = subprocess.call(cmd)

        if return_code == 0:
            super(RepoView, self).save(*args, **kwargs)

class GuidMapping(models.Model):

    #These choices are provide to help with consistentcy only,
    #really the values could be anything.
    arch_choices = (
        ('i386','i386'),
        ('x86_64','x86_64'),
        ('ANY','ANY'),
        )

    type_choices = (
        ('os','os'),
        ('updates','updates'),
        ('addons','addons'),
        ('extras','extras'),
        ('contrib','contrib'),
        ('ANY','ANY')
        )

    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    distro = models.CharField(max_length=25,null=False, blank=False,
        default="ANY")

    releasever = models.CharField(max_length=10,null=False, blank=False,
        default="ANY")

    repo_type = models.CharField(max_length=25, choices=type_choices,
        null=False, blank=False, default="ANY")

    basearch = models.CharField(max_length=10, choices=arch_choices,
        null=False, blank=False, default="ANY")

    guid = models.CharField(max_length=255,null=False, blank=False,
        default="ANY")

    repo_view = models.ForeignKey('RepoView')
    last_modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

