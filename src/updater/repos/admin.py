from django.contrib import admin
from repos.models import Repo, RepoView, GuidMapping

class ReposAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'file_system_location', 'last_modified')

    readonly_fields = (
        'repo_meta_data_location',
        )

class ReposViewAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'main_repo', 'last_modified')
    readonly_fields = [
        'repo_file_list',
        ]

    def repo_file_list(self, obj):
        files = obj.get_file_list()
        print files
        if len(files) == 1:
            #There is an error, just return it
            return files[0]

        results = "<BR><ul>"
        for f in files:
            results+="<li>%s</li>"%(f)

        results+="</uL>"
        return results

    repo_file_list.allow_tags = True

class GuidMappingAdmin(admin.ModelAdmin):
    list_display = ('name',
        'description',
        'distro',
        'releasever',
        'repo_type',
        'basearch',
        'guid',
        'repo_view',
        'last_modified')

    list_filter = (
        'last_modified',
        'repo_view',
        'guid',
        )

admin.site.register(Repo, ReposAdmin)
admin.site.register(RepoView, ReposViewAdmin)
admin.site.register(GuidMapping, GuidMappingAdmin)

