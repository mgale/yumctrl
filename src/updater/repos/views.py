# Create your views here.
import os
import mimetypes
from django.template import Context, loader
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from repos.models import Repo, RepoView, GuidMapping

#distro = 2
#releasever = 3
#repo_type = 4
#basearch = 5
#guid = 6
#file_name | repodata

def download_file(request):

    args = request.META['PATH_INFO'].split("/")

    if len(args) < 7:
        return HttpResponse(400)

    file_name = args[-1]
    disk_file = None
    template_id = None

    search_parms = [
        [ 'distro', args[2] ],
        [ 'releasever', args[3] ],
        [ 'repo_type', args[4] ],
        [ 'basearch', args[5] ],
        [ 'guid', args[6] ],
        ]

    for i in range(len(search_parms)-1, -2, -1):
        kwargs = {}
        for k,v in search_parms:
            kwargs[k] = v

        matches = GuidMapping.objects.filter(**kwargs)
        if len(matches) > 0:
            template_id = matches[0].repo_view.id
            break

        if i >= 0:
            search_parms[i][1] = "ANY"

    if template_id is None:
        return HttpResponse(412)

    repo_view = RepoView.objects.get(pk=template_id)

    if args[7] == "repodata":
        print "Repo request"
        disk_file = "%s/repodata/%s"%(repo_view.get_repo_meta_dir(), file_name)
    else:
        #We do not perform security checks for specific files!!!
        disk_file = "%s/%s"%(repo_view.main_repo.file_system_location,
            file_name)

    response = HttpResponse(FileWrapper(open(disk_file)),
            content_type=mimetypes.guess_type(disk_file)[0]
            )
    response['Content-Length'] = os.path.getsize(disk_file)
    response['Content-Disposition'] = "attachment; filename=%s"%(file_name)
    return response

