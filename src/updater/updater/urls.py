from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'updater.views.home', name='home'),
    # url(r'^updater/', include('updater.foo.urls')),
    # repo/centos/$releasever/os/$basearch/
    #/repo/centos/5.2/os/x86_64/1234-5678-9011-abcd/repodata/repomd.xml
    #/repo/centos/5.2/os/x86_64/1234-5678-9011-abcd/nms-zlib-1.2.7-1.x86_64.rpm
    url(r'^repo/', 'repos.views.download_file'),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
